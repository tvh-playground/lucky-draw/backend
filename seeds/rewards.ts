import { Knex } from "knex"

export async function seed(knex: Knex): Promise<void> {
  await knex("rewards").del()

  await knex("rewards").insert([
    { id: 1, remainder: 5 },
    { id: 2, remainder: 25 },
    { id: 3, remainder: 30 },
    { id: 4, remainder: 40 }
  ])
}
