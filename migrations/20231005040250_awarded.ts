import { Knex } from "knex"

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('awarded', (table) => {
    table.increments().unsigned().notNullable()
    table.string('code', 10).notNullable().unique()
    table.integer('rewardId').unsigned().notNullable()
      .references('id').inTable('rewards')
    table.timestamp('created_at').notNullable().defaultTo(knex.fn.now())
    table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now())
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('awarded')
}
