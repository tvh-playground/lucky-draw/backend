import { Knex } from "knex"

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('rewards', (table) => {
    table.increments().unsigned().notNullable()
    table.integer('remainder').unsigned().notNullable().defaultTo(0)
    table.timestamp('created_at').notNullable().defaultTo(knex.fn.now())
    table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now())
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('rewards')
}
