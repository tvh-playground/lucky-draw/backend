import windston from 'winston'
import dayjs from 'dayjs'

const transports = [new windston.transports.Console()]

const format = windston.format.combine(
  windston.format.colorize(),
  windston.format.timestamp(),
  windston.format.printf(({ timestamp, level, message }) =>
    `${level}[${dayjs(timestamp).format('DD MMM YY HH:mm:ss')}]: ${message}`
  )
)

export default windston.createLogger({
  transports,
  format
})
