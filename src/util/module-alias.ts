import moduleAlias from 'module-alias'
import { join } from 'path'

const rootDir = join(process.cwd(), process.env.NODE_ENV === 'production' ? '.dist' : 'src')
moduleAlias()
moduleAlias.addAlias('@api', join(rootDir, './api'))
moduleAlias.addAlias('@database', join(rootDir, './database'))
moduleAlias.addAlias('@logger', join(rootDir, './util/logger'))
moduleAlias.addAlias('@server', join(rootDir, './util/server'))
