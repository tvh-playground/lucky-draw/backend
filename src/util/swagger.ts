import swaggerUi from 'swagger-ui-express'

const openapi = '3.1.0'

const info: swaggerUi.JsonObject['info'] = {
  title: 'Lucky Draw',
  description: 'BackEnd system to generate result for lucky draw.',
  versionss: '1.0'
}

const produces: swaggerUi.JsonObject['produces'] = ['application/json']

const paths: swaggerUi.JsonObject['path'] = {
  '/api/draw': {
    post: {
      description: 'Return a random reward id',
      responses: {
        200: {
          description: 'Randomed reward id',
          schema: {
            $ref: '#/definitions/User'
          }
        },
        400: {
          description: 'Randomed reward id',
          schema: {
            $ref: "#/definitions/featureListFeaturesResponse"
          }
        }
      }
    }
  }
}

const definitions = {
  RewardInfo: {
    properties: {}
    // type: 'object',
    // properties: {
    //   app_id: {
    //     type: "string",
    //     format: "string"
    //   },
    //   args: {
    //     type: "array",
    //     items: {
    //       type: "string",
    //       format: "string"
    //     }
    //   },
    //   "enabled": {
    //     "type": "boolean",
    //     "format": "boolean"
    //   },
    //   "extra_data": {
    //     "$ref": "#/definitions/protobufStruct",
    //     "title": "extra data can be any json data\nFor parnter(特权专区) feature it shold be\nrepeated PartnerExtra extra_data = 10"
    //   },
    //   "method": {
    //     "$ref": "#/definitions/featureFeatureFetchMethod"
    //   },
    //   "requirements": {
    //     "type": "array",
    //     "items": {
    //       "type": "string",
    //       "format": "string"
    //     }
    //   },
    //   "type": {
    //     "$ref": "#/definitions/featureFeatureType"
    //   },
    //   "updated_at": {
    //     "type": "string",
    //     "format": "date-time"
    //   },
    //   "uuid": {
    //     "type": "string",
    //     "format": "string"
    //   }
    // }
  }
}

const option: swaggerUi.JsonObject = {
  openapi: '3.1.0',
  info,
  produces,
  paths,
  definitions
}

export default {
  serve: swaggerUi.serve,
  setup: swaggerUi.setup(option)
}