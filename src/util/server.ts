import express from 'express'
import logger from '@logger'
import draw from '@api/draw'
import swagger from './swagger'

const app = express()
const portNumber = process.env.SERVER_PORT || 3000

export default () => {
  app.use('/api-docs', swagger.serve, swagger.setup)
  app.use('/api/draw', draw)

  app.listen(portNumber, () => {
    logger.info(`Listening to ${portNumber}.`)
  })
}
