import { Router } from 'express'
import database from '@database'
import logger from '@logger'

interface Reward {
  id: number
  remainder: number
  updated_at: string
}

const characters = 'ABCDEFGHJKLMNPQRSTUVWXYZ0123456789'
const router = Router()

router.post('/', async (req, res) => {
  let prize
  try {
    await database
      .knex
      .transaction(async (trx) => {
        const rewardList = await trx<Reward>('rewards')
          .select('id', 'remainder')
          .forUpdate()
        if (rewardList.length < 1) {
          throw new Error('All rewards redeemed.')
        }
  
        const rewardPossibility: Array<number> = []
        const rewardRemainder: Record<number, number> = {}
        for (let i = 0; i < rewardList.length; i++) {
          if (rewardList[i].remainder <= 0) { continue }
          rewardPossibility.push(
            ...Array(rewardList[i].remainder).fill(rewardList[i].id)
          )
          rewardRemainder[rewardList[i].id] = rewardList[i].remainder
        }
  
        const index = Math.floor(Math.random() * rewardPossibility.length)
        prize = rewardPossibility.sort(() => Math.random() - 0.5)[index]
  
        await trx<Reward>('rewards')
          .update({
            remainder: rewardRemainder[prize] - 1,
            updated_at: database.knex.raw('NOW()')
          })
          .forUpdate()
          .where('id', prize)
      })
  } catch (err: any) {
    logger.error(err)
    res.status(400).json({ message: err.message })
    return
  }

  if (!prize) { return }
  const code = Array(10)
    .fill(0)
    .map(() => characters[Math.floor(Math.random() * characters.length)])
    .join('')
  await database
    .knex('awarded')
    .insert({
      code,
      rewardId: prize,
      created_at: database.knex.raw('NOW()'),
      updated_at: database.knex.raw('NOW()')
    })

  res.json({ prize })
})

export default router
