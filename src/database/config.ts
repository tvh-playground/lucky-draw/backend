export default {
  client: 'postgresql',
  connection: {
    host: 'database',
    database: 'lucky_draw',
    user: 'postgres',
    password: 'postgres'
  },
  pool: {
    min: 2,
    max: 10
  },
  migrations: {
    tableName: '__migrations',
    directory: '../../migrations'
  },
  seeds: {
    directory: '../../seeds'
  }
}
