import knex, { Knex } from 'knex'
import knexfile from '../database/config'

export class DatabaseConnection {
  private _knex: Knex
  get knex () { return this._knex }

  constructor (_knexfile = knexfile) {
    this._knex = knex(_knexfile)
  }
}

export default new DatabaseConnection()
